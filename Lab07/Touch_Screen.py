'''
@file Touch_Screen.py

@brief This file contains a class Touch_Screen which is designed to read a touch location off of a touch screen.

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import Pin
from pyb import ADC


class Touch_Screen:
    '''
    @brief     The Touch_Screen class creates an object that can read the location 
               of a touch input from a resistive touch screen
    '''
    
    def __init__(self, pin_xp, pin_xm, pin_yp, pin_ym, width, length, center_x, center_y ):
        '''
        @brief Initializes the touchsceen object, defining the pins that will be read from to determine position
        @param pin_xp Pin object connected to the positive x on the touchscreen
        @param pin_xm Pin object connected to the negative x on the touchscreen
        @param pin_yp Pin object connected to the positive y on the touchscreen
        @param pin_ym Pin object connected to the negative y on the touchscreen
        @param width Integer relating the width (x direction) in mm
        @param length Integer relating the length (y direction) in mm
        @param center_x Integer value for the center of the x coordinates in mm
        @param center_y Integer value for the center of the y coordinates in mm
        '''
        ## Pin object for the positive x lead of the touchscreen
        self.pin_xp = Pin(pin_xp)
        
        ## Pin object for the negative x lead of the touchscreen
        self.pin_xm = Pin(pin_xm)
        
        ## Pin object for the positive y lead of the touchscreen
        self.pin_yp = Pin(pin_yp)
        
        ## Pin object for the negative y lead of the touchscreen
        self.pin_ym = Pin(pin_ym)
        
        ## Integer of touchscreen width
        self.width = width
        
        ## Integer of touchscreen length
        self.length = length
        
        ## Integer of touchscreen center in x direction
        self.center_x = center_x
        
        ## Integer of touchscreen center in y direction
        self.center_y = center_y
        
    def scanX(self):
        '''
        @brief Returns x coordinate of touch location
        @details This method creates push pull pin objects for the x pins and sets
                 their values to high and low. It then initializes the y pins as analog 
                 and creates and ADC using the y minus pin. Finally, it waits 5 us for 
                 the signal to settle, then reads the ADC value.
        '''
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)
        self.pin_xp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_ym.init(mode=Pin.ANALOG)
        self.pin_yp.init(mode=Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        utime.sleep_us(4)
        return ((ADC_ym.read()-200)/3580*self.width-self.center_x)

    def scanY(self):
        '''
        @brief Returns y coordinate of touch location
        @details This method creates push pull pin objects for the y pins and sets
                 their values to high and low. It then initializes the x pins as analog 
                 and creates and ADC using the x minus pin. Finally, it waits 5 us for 
                 the signal to settle, then reads the ADC value.
        '''
        self.pin_ym.init(mode=Pin.OUT_PP, value = 0)
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_xm.init(mode=Pin.ANALOG)
        self.pin_xp.init(mode=Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        utime.sleep_us(4)
        return ((ADC_xm.read()-375)/3245*self.length-self.center_y)
        
    def scanZ(self):
        '''
        @brief Returns z coordinate of touch location
        @details This method creates push pull pin objects for the yp and xm pins and sets
                 their values to high and low respectively. It then initializes the xp and ym pins as analog 
                 and creates and ADC using the xp pin. Finally, it waits 5 us for 
                 the signal to settle, then reads the ADC value.
        '''
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_xp.init(mode=Pin.ANALOG)
        self.pin_ym.init(mode=Pin.ANALOG)
        ADC_xp = ADC(self.pin_xp)
        utime.sleep_us(4)
        return (ADC_xp.read()/1000)        
        
    def scanPosition(self):
        '''
        @brief Returns the x, y, and z locations of a touch
        @details This method essentially combines the scanX, scanY and scanZ functions, but arranges 
                 them so that not all pins have to be re-initialized between reading x, y, and z values.
        '''   
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)
        self.pin_xp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_ym.init(mode=Pin.ANALOG)
        self.pin_yp.init(mode=Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        utime.sleep_us(4)
        x = ((ADC_ym.read()-200)/3580*self.width-self.center_x)
    
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(self.pin_xp)
        utime.sleep_us(4)
        z = (ADC_xp.read()/1000)  
    
        self.pin_ym.init(mode=Pin.OUT_PP, value = 0)
        self.pin_xm.init(mode=Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        utime.sleep_us(4)
        y = ((ADC_xm.read()-375)/3245*self.length-self.center_y)
        
        return(x, y, z)

if __name__ == "__main__":
    # Execute if run as a main file
    TS = Touch_Screen (Pin.cpu.A1, Pin.cpu.A0, Pin.cpu.A7, Pin.cpu.A6, 184, 102, 92, 51 )
    runs = 1
    start_time = utime.ticks_us()
    for runs in range(1000):
        TS.scanPosition()
    print("Average Runtime = " + str(utime.ticks_diff(utime.ticks_us(), start_time)/runs))