'''
@file Touch_Screen.py

@brief 

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import Pin
from pyb import ADC


class Touch_Screen:
    '''
    @brief      
    '''
    
    def __init__(self, pin_xp, pin_xm, pin_yp, pin_ym, width, length, center_x, center_y ):
        '''
        @brief
        @param 
        '''
        self.pin_xp = Pin(pin_xp)
        
        self.pin_xm = Pin(pin_xm)
        
        self.pin_yp = Pin(pin_yp)
        
        self.pin_ym = Pin(pin_ym)
        
        self.width = width
        
        self.length = length
        
        self.center_x = center_x
        
        self.center_y = center_y
        
    def scanX(self):
        '''
        @brief
        @details
        '''
        self.pin_xm.init(mode=Pin.OUT_PP, value = 0)
        self.pin_xp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_ym.init(mode=Pin.ANALOG)
        self.pin_yp.init(mode=Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        utime.sleep_us(4)
        return ((ADC_ym.read()-200)/3580*self.width-self.center_x)

    def scanY(self):
        '''
        @brief
        @details
        '''
        self.pin_ym.init(mode=Pin.OUT_PP, value = 0)
        self.pin_xm.init(mode=Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        utime.sleep_us(4)
        return ((ADC_xm.read()-375)/3245*self.length-self.center_y)
        
    def scanZ(self):
        '''
        @brief
        @details
        '''
        self.pin_yp.init(mode=Pin.OUT_PP, value = 1)
        self.pin_xp.init(mode=Pin.ANALOG)
        ADC_xp = ADC(self.pin_xp)
        utime.sleep_us(4)
        return (ADC_xp.read()/1000)        
        
    def scanPosition(self):
        '''
        @brief
        @details
        '''   
        return(self.scanX(), self.scanZ(), self.scanY())

if __name__ == "__main__":
    
    TS = Touch_Screen (Pin.cpu.A1, Pin.cpu.A0, Pin.cpu.A7, Pin.cpu.A6, 184, 102, 92, 51 )
    runs = 1
    start_time = utime.ticks_us()
    for runs in range(1000):
        TS.scanPosition()
    print("Average Runtime = " + str(utime.ticks_diff(utime.ticks_us(), start_time)/runs))