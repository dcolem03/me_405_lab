# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 13:44:35 2021

@author: danie
"""

import pyb
from pyb import Pin
from pyb import ADC

PIN_xm = Pin (Pin.cpu.A0)
PIN_xp = Pin (Pin.cpu.A1)
ADC_ym = ADC(Pin.cpu.A6)
PIN_ym = Pin (Pin.cpu.A6, mode = Pin.IN)
PIN_xm.init(mode=Pin.OUT_PP, value = 0)
PIN_xp.init(mode=Pin.OUT_PP, value = 1)
PIN_ym.init(mode=Pin.ANALOG)
ADC_ym = ADC(PIN_ym)
#set yp as an analoh input
while True:
    print(ADC_ym.read()/4096)
