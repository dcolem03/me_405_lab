'''
@file Lab_0x01.py

@brief Vending machine finite state machine

@details A Vending machine task contains several general use functions as well as a run function.
         The Run function is a finite state machine that allows users to input integers to represent
         money and letter values to select a drink.

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import time
import keyboard

class TaskVendingMachine:
    '''
    @brief      Vending machine task.
    
    @details    
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_INP    = 1
    
    ## Waiting for response state
    S2_COIN_INSERTED    = 2
    
    ## Waiting for response state
    S3_DRINK_SELECTED    = 3
    
    ## Waiting for response state
    S4_EJECT   = 4
    
    ## Waiting for response state
    S5_BORED    = 5

    def __init__(self, taskNum, interval, dbg):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        print('test')
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.thread_time_ns()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## Reference point to compare timer
        self.timer_start = self.start_time
        
        ## A list to cary the current ballance input from the customer
        self.balance = [0, 0, 0, 0, 0, 0, 0, 0]
        
        ## A variable to carry the users input        
        self.pushed_key = " "
        
        ## A variable to store the price of cuke
        self.c_price = 1.00
        
        ## A variable to store the price of popsi
        self.p_price = 1.20
        
        ## A variable to store the price of spryte
        self.s_price = .85
        
        ## A variable to store the price of dr. pupper
        self.d_price = 1.10
        
        ## A variable to store the remainder
        self.remainder = None
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        @brief Runs one iteration of the vending machine task
        @details The run FSM allows the user to deposit money and purchase drinks 
                 like from a vending machine. The function also allows the balance
                 to be ejected at any time.
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = time.thread_time_ns()
        if self.curr_time - self.next_time >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                keyboard.on_press (self.on_keypress)
                print('Welcome! \n'
                      'Please enter coins or bills using the inputs:\n'
                      '1 = penny\n'
                      '2 = nickle\n'
                      '2 = dime\n'
                      '3 = quarter\n'
                      '4 = dollar\n'
                      '5 = 5 dollars\n'
                      '6 = 10 dollars\n'
                      '7 = 20 dollars\n'
                      '\n'
                      'Select a drink using the inputs:\n'
                      'c = Cuke\n'
                      'p = Popsi\n'
                      's = Spryte\n'
                      'd = Dr. Pupper\n'
                      '\n'
                      'At any time, input e to eject your current balance')
                self.transitionTo(self.S1_WAIT_FOR_INP)
            
            elif(self.state == self.S1_WAIT_FOR_INP):
                self.printTrace()
                # Run State 1 Code
                try:
                # If a key has been pressed, check if it's a key we care about
                    if self.pushed_key:
                        if self.pushed_key == '0' or self.pushed_key == '1' or self.pushed_key == '2' or self.pushed_key == '3' or self.pushed_key == '4' or self.pushed_key == '5' or self.pushed_key == '6' or self.pushed_key == '7':
                            self.transitionTo(self.S2_COIN_INSERTED)  
                        elif self.pushed_key == 'c' or self.pushed_key == 'p' or self.pushed_key == 's' or self.pushed_key == 'd':
                            self.transitionTo(self.S3_DRINK_SELECTED)                             
                        elif self.pushed_key == 'e':
                            self.transitionTo(self.S4_EJECT)
                        else:
                            print('Invalid input, please try again')
                except:
                    print('error')
                
                # If 
                if (self.curr_time - self.timer_start)/1000000000 >= 10:
                    self.transitionTo(self.S5_BORED) 
                    
            
            elif(self.state == self.S2_COIN_INSERTED):
                self.printTrace()
                # Run State 2 Code
                self.balance[int(self.pushed_key)] +=1
                print(self.balance)
                
                self.pushed_key = ""
                self.timer_start = self.curr_time
                self.transitionTo(self.S1_WAIT_FOR_INP)
                    
            elif(self.state == self.S3_DRINK_SELECTED):
                self.printTrace()
                # Run State 3 Code
                if self.pushed_key == 'c':
                    self.remainder = self.getChange(self.c_price, self.balance)
                    if type(self.remainder) == list:
                        print('Enjoy your Cuke!')
                        self.balance = self.remainder
                        self.transitionTo(self.S4_EJECT)
                    else:
                        print(self.remainder)
                        self.pushed_key = ""
                        self.timer_start = self.curr_time
                        self.transitionTo(self.S1_WAIT_FOR_INP)
                elif self.pushed_key == 'p':
                    self.remainder = self.getChange(self.p_price, self.balance)
                    if type(self.remainder) == list:
                        print('Enjoy your Popsi!')
                        self.balance = self.remainder
                        self.transitionTo(self.S4_EJECT)
                    else:
                        print(self.remainder)
                        self.pushed_key = ""
                        self.timer_start = self.curr_time
                        self.transitionTo(self.S1_WAIT_FOR_INP) 
                elif self.pushed_key == 's':
                    self.remainder = self.getChange(self.s_price, self.balance)
                    if type(self.remainder) == list:
                        print('Enjoy your Spryte!')
                        self.balance = self.remainder
                        self.transitionTo(self.S4_EJECT)
                    else:
                        print(self.remainder)
                        self.pushed_key = ""
                        self.timer_start = self.curr_time
                        self.transitionTo(self.S1_WAIT_FOR_INP) 
                else:
                    self.remainder = self.getChange(self.d_price, self.balance)
                    if type(self.remainder) == list:
                        print('Enjoy your Dr. Pupper!')
                        self.balance = self.remainder
                        self.transitionTo(self.S4_EJECT)
                    else:
                        print(self.remainder)
                        self.pushed_key = ""
                        self.timer_start = self.curr_time
                        self.transitionTo(self.S1_WAIT_FOR_INP) 

            elif(self.state == self.S4_EJECT):
                self.printTrace()
                # Run State 6 Code
                
                print( 'Your Balance ' + str(self.balance) + ' has been returned')
                self.balance = [0, 0, 0, 0, 0, 0, 0, 0]
                self.pushed_key = ""
                self.timer_start = self.curr_time
                self.transitionTo(self.S1_WAIT_FOR_INP)
                
            elif(self.state == self.S5_BORED):
                self.printTrace()
                # Run State 7 Code
                print('Enjoy a refreshing drink today!')
                self.timer_start = self.curr_time
                self.transitionTo(self.S1_WAIT_FOR_INP)
            else:
                # error handling
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time - self.start_time)
            print(str)
            
    def getChange(self, price, payment):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        @param price A double reflecting the price of the object
        @param payment A list containing information about the payment provided
        @return Either the change from the transaction or an error message if there are insufficient funds
        '''
    
        # convert the price from decimal dollars to integer cents
        intern_price = int(price*100)
        # convert the payment into integer cents
        intern_payment = payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment [5] + 1000*payment[6] + 2000*payment[7]
        # calculate the remainder in integer cents
        remainder = intern_payment-intern_price
        # create change list
        if remainder >=0:
            change = [0,0,0,0,0,0,0,0]
            # Iterate through every change value from $20 bills down the pennies 
            # to determine the simplest way to return change
            (change[7],remainder) = divmod(remainder,2000)
            (change[6],remainder) = divmod(remainder,1000)
            (change[5],remainder) = divmod(remainder,500)
            (change[4],remainder) = divmod(remainder,100)
            (change[3],remainder) = divmod(remainder,25)
            (change[2],remainder) = divmod(remainder,10)
            (change[1],remainder) = divmod(remainder,5)
            change[0] = remainder
            # convert the list to a tuple and return it
            return change
        else:
            return "Insufficient funds, please insert more!"
    
    def on_keypress (self, thing):
        """ 
        @brief Callback which runs when the user presses a key.
        """
        self.pushed_key = thing.name




