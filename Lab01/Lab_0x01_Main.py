'''
@file Lab_0x01_Main.py

@brief main file that creates and runs a Vending Machine object

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

# Import vending machine task
from Lab_0x01_FSM import TaskVendingMachine

# Set interval
interval = 1000000

# Create Vending machine task
VM_Task = TaskVendingMachine(1, interval, False)


# Continuously run VM_Task
while True:                           
    VM_Task.run()