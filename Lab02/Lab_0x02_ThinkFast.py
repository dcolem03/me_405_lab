'''
@file Lab_0x02_ThinkFast.py

@brief A program that can test the user's reaction time using the built in LED and button on the nucleo

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
# Import any required modules
import pyb
import micropython
import random

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Integer to carry the total number of microseconds
total_Time = 0

## Integer to carry the total number of tests
count = 0

## Pin object to use for user button.
Button = pyb.Switch()

##
def interrupt():
    '''
        @brief Runs as an interupt on a button press
        @details When the button is press, the current code is interrupted and 
                 this code is run instead. It adds time to the total time count and
                 adds number of counts so that the average reaction time can be
                 calculated following a keyboard interrupt.
        '''
    global total_Time
    total_Time += myTimer.counter()
    global count
    count += 1
    LED.low()

Button.callback(interrupt)    

## Pin object to use for LED. Attached to PA5
LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Timer object to keep track of time in microseconds
myTimer = pyb.Timer(2,prescaler = 79, period=0x7FFFFFFF)

# Main program loop
while True:
    try:
        pyb.delay(random.randint(2000,3000))  
        LED.high()
        myTimer.counter(0)
        pyb.delay(1000)
    except KeyboardInterrupt:
        # If reaction time data has been collected, display it, otherwise, display an error message
        if count>0:
            print('Your average reaction time is: ' + str(round(total_Time/count)) + ' microseconds')
        else:
            print('No times were collected, Please try again!')
        # Once ctr+c is pressed, break the while loop
        break