

%% Symbolic Form
clear all
close all

% Create symbolic term corresponding to each variable
syms mb mp rm rb rp rc rg lr lp ib ip b g T x xd xdd th thd thdd

% Create 
Q = [x; th];
Qd = [xd; thd];

% M from the provided Lab 5 solution
M = [ -(mb*rb^2 + mb*rc*rb + ib)/rb, -(ib*rb + ip*rb + mb*rb^3 + mb*rb*rc^2 + 2*mb*rb^2*rc + mp*rb*rg^2 + mb*rb*x^2)/rb;
    -(mb*rb^2 + ib)/rb, -(mb*rb^3 + mb*rc*rb^2 + ib*rb)/rb];

% F from the provided Lab 5 solution
F = [b*thd - g*mb*(th*(rb+rc)*x) + T*lp/rm + 2*mb*thd*x*xd - g*mp*rg*th;
    -mb*rb*x*thd^2 - g*mb*rb*th];

% Solve for xdd and thdd in the form of qdd
Qdd = inv(M)*F;

X = [Q; Qd];

Xd = formula([Qd; Qdd]);

% Solve the jacobian matrix
Jx = jacobian(Xd,X);
Ju = jacobian(Xd,[T]);




%% Solution

clear all;
close all;
sympref('FloatingPointOutput',true);

% Inputs
rm = .060; % [m]
lr = .050; % [m]
rb = .0105; % [m]
rg = .042; % [m]
lp = .110; % [m]
rp = .0325; % [m]
rc = .050; % [m]
mb = .030; % [kg]
mp = .400; % [kg]
ib = 2/5*mb*rb^2; % [kg*m^2]
ip = 1.88 * 10^6/10^9; % [kg*m^2]
b = .010; % [N*m*s/rad]
g = 9.81; % [m/s^2]

% Create symbolic variables for everything time dependant
syms T x xd xdd th thd thdd

Q = [x; th];
Qd = [xd; thd];

% M from the provided Lab 5 solution
M = [ -(mb*rb^2 + mb*rc*rb + ib)/rb, -(ib*rb + ip*rb + mb*rb^3 + mb*rb*rc^2 + 2*mb*rb^2*rc + mp*rb*rg^2 + mb*rb*x^2)/rb;
    -(mb*rb^2 + ib)/rb, -(mb*rb^3 + mb*rc*rb^2 + ib*rb)/rb];

% F from the provided Lab 5 solution
F = [b*thd - g*mb*(th*(rb+rc)+x) + T*lp/rm + 2*mb*thd*x*xd - g*mp*rg*th;
    -mb*rb*x*thd^2 - g*mb*rb*th];

Qdd = inv(M)*F;

X = [Q; Qd];

Xd = formula([Qd; Qdd]);

% Find the jacobian matrix
Jx = jacobian(Xd,X);
Ju = jacobian(Xd,[T]);

% Solve the jacobian at the initial conditions of X = 0, u = 0
A = double(formula(subs(Jx, [x, xd, th, thd, T], [0, 0, 0, 0, 0])));
B = double(formula(subs(Ju, [x, xd, th, thd, T], [0, 0, 0, 0, 0])));

%% Simulink a

close all;

% Conditions
t_sim = 1; % [s]
init = [0; 0; 0; 0];
T0 = 0;
t_T = 0;

% Run the simulation and plot the results

sim('Lab06_Sim_OL');

subplot(2,2,1);
plot (ans.tout, ans.X(:,1));
title ('Ball Position vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Position [m]');

subplot(2,2,2);
plot (ans.tout, ans.X(:,2));
title ('Platform Angle vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angle [rad]');

subplot(2,2,3);
plot (ans.tout, ans.X(:,3));
title ('Ball Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Velocity [m/s]');

subplot(2,2,4);
plot (ans.tout, ans.X(:,4));
title ('Platform Angular Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angular Velocity [rad/s]');

%% Simulink b

% Conditions
t_sim = .4; % [s]
init = [.05; 0; 0; 0];
T0 = 0;
t_T = 0;

% Run the simulation and plot the results

sim('Lab06_Sim_OL');
 
figure();

subplot(2,2,1);
plot (ans.tout, ans.X(:,1));
title ('Ball Position vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Position [m]');

subplot(2,2,2);
plot (ans.tout, ans.X(:,2));
title ('Platform Angle vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angle [rad]');

subplot(2,2,3);
plot (ans.tout, ans.X(:,3));
title ('Ball Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Velocity [m/s]');

subplot(2,2,4);
plot (ans.tout, ans.X(:,4));
title ('Platform Angular Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angular Velocity [rad/s]');

%% Simulink c

% Conditions
t_sim = .4; % [s]
init = [0; 5*2*pi()/360; 0; 0];
T0 = 0;
t_T = 0;

% Run the simulation and plot the results

sim('Lab06_Sim_OL');
 
figure();

subplot(2,2,1);
plot (ans.tout, ans.X(:,1));
title ('Ball Position vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Position [m]');

subplot(2,2,2);
plot (ans.tout, ans.X(:,2));
title ('Platform Angle vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angle [rad]');

subplot(2,2,3);
plot (ans.tout, ans.X(:,3));
title ('Ball Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Velocity [m/s]');

subplot(2,2,4);
plot (ans.tout, ans.X(:,4));
title ('Platform Angular Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angular Velocity [rad/s]');

%% Simulink d

% Conditions
t_sim = .4; % [s]
init = [0; 0; 0; 0];
T0 = 1;
t_T = 0.001;

% Run the simulation and plot the results

sim('Lab06_Sim_OL');
 
figure();

subplot(2,2,1);
plot (ans.tout, ans.X(:,1));
title ('Ball Position vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Position [m]');

subplot(2,2,2);
plot (ans.tout, ans.X(:,2));
title ('Platform Angle vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angle [rad]');

subplot(2,2,3);
plot (ans.tout, ans.X(:,3));
title ('Ball Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Velocity [m/s]');

subplot(2,2,4);
plot (ans.tout, ans.X(:,4));
title ('Platform Angular Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angular Velocity [rad/s]');

%% Simulink CL

% Conditions
t_sim = 20; % [s]
init = [0.05; 0; 0; 0];
T0 = 0;
t_T = 0;
K = [-.3, -.2, -.05, -.02 ];

% Run the simulation and plot the results

sim('Lab06_Sim_CL');
 
figure();

subplot(2,2,1);
plot (ans.tout, ans.X(:,1));
title ('Ball Position vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Position [m]');

subplot(2,2,2);
plot (ans.tout, ans.X(:,2));
title ('Platform Angle vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angle [rad]');

subplot(2,2,3);
plot (ans.tout, ans.X(:,3));
title ('Ball Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Ball Velocity [m/s]');

subplot(2,2,4);
plot (ans.tout, ans.X(:,4));
title ('Platform Angular Velocity vs Time');
xlabel ('Time [sec]');
ylabel ('Platform Angular Velocity [rad/s]');
