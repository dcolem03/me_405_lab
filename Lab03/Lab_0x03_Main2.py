'''
@file Lab_0x03_Main.py

@brief main file that runs on startup on the nucleo

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
from pyb import UART
import array
import micropython

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

button = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.OUT_PP)

adc = pyb.ADC(pyb.Pin.board.PA0)

## Timer object to keep track of time in microseconds
myTimer = pyb.Timer(2,freq = 200000)

buffer = array.array ('H', (0 for index in range (1000)))

ser = UART(2)

press = False

def myCallback(self):
    '''
        @brief Runs as an interupt on a button press
        @details 
    '''
    global press
    press = True

extint = pyb.ExtInt(button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, myCallback)

n = 0

# Continuously run the encoder task
while True:
    try:
        adc.read_timed(buffer, myTimer)
        if True: #ser.readchar() == 71:
            if press:
                for j in range (1000):
                    print('{:}, {:}'.format(j/200000, buffer[j]))
                press = False
    except KeyboardInterrupt:
        break
    
    
        
