'''
@file Lab_0x03_Main.py

@brief main file that runs on startup on the nucleo

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
from pyb import UART
import array
import micropython

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object for the user button
button = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.OUT_PP)

## ADC object on pin A0
adc = pyb.ADC(pyb.Pin.board.PA0)

## Timer object to keep track of time in microseconds
myTimer = pyb.Timer(2,freq = 200000)

## Array to hold ADC's buffer values
buffer = array.array ('H', (0 for index in range (20000)))

## Serial object to monitor for user input
ser = UART(2)

press = False

def myCallback(self):
    '''
        @brief Runs as an interupt on a button press
        @details When the user button is pressed, this interrupt runs, fills the buffer with the ADC and turns a boolean to true
    '''
    pyb.delay(50)
    adc.read_timed(buffer, myTimer)
    global press
    press = True

## Interrupt designed to trigger on the falling edge of a signal with myCallback() function
extint = pyb.ExtInt(button, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, myCallback)

n = 0

# Continuously check for updates
while True:
    try:
        # Wait for input from computer
        if ser.any():
            # Wait for button press
            if press:
                # Find location of button press in the buffer
                for i in range (len(buffer)-1):
                    if abs(buffer[i+1]-buffer[i])>50:
                        n = i
                        break
                    else: 
                        n =0
                # Print values that surround the button press
                for j in range (1000):
                    print('{:}, {:}'.format((j+n-50)/200000, buffer[j+n-50]))
                press = False
            else:
                pass
    except KeyboardInterrupt:
        break
    
    
        
