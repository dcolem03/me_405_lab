'''
@file Lab_0x03_UI.py

@brief This script acts as the Spyder user interface for lab 3

@details This script is designed to run on a computer while the companion main file runs on a nucleo.
         When this script is run, the user is prompted to enter G to allow the nucleo script to start monitoring
         the button voltage. After a G is entered, the button must be pressed quickly in order to ensure a good plot.

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import serial
import matplotlib.pyplot as plt
import time
import csv

# Create a serial port for nucleo communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=0.5)


# run twice
if True:
    try:
        # Create Time and Position lists
        Time = []
        Voltage = []
        
        # Ask for user input
        inv = input('Please enter "G" to start monitoring button response: ')
        
        #Check if user input is valid
        if inv == 'G':
            print('Thank you, button is now being monitored.')
            # Send uesr input to nucleo
            ser.write(str(inv).encode('ascii'))
            
            # Wait for data to be collected and sent
            time.sleep(10)
            # Organize data into the proper data structures
            while ser.in_waiting > 0:
                line_string = ser.readline().decode('ascii')
                line_list = line_string.strip().split(',')
                                                      
                Time.append(float(line_list[0]))
                Voltage.append(float(line_list[1]))
                
            # Translate time to values relative to 0
            for i in range (len(Time)-1):
                Time[i+1] = Time[i+1]-Time[0]
            Time[0] = 0
            # Translate Voltage to volt units
            for i in range (len(Voltage)):
                Voltage[i] = Voltage[i]*3.3/4096

            # Plot Results
            plt.plot(Time,Voltage)
            plt.xlabel('Time [s]')
            plt.ylabel('Pin Voltage [V]')
            plt.title('Button Step Response')
            plt.show()  
        
            # Create csv file
            with open('Lab_3_Data.csv', 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(Time)
                writer.writerow(Voltage)
        
        else:
            print('Invalid input, please try again')
    except KeyboardInterrupt:
        print('1')
    except: 
        print('There was an error')
    
    
ser.close()
